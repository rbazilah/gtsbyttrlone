//page component

import React from "react"
import Container from "../components/container"
import styles from "./about-css-modules.module.css"

console.log(styles)

//component
const User = props => (
    <div className={styles.user}>
        <img src={props.avatar} className={styles.avatar} alt="" />
        <div className={styles.description}>
            <h2 className={styles.username}>{props.username}</h2>
            <p className={styles.excerpt}>{props.excerpt}</p>
        </div>
    </div>
)

export default function About() {
    return (
        <Container>
            <h1>About CSS Modules</h1>
            <p>CSS Module are cool</p>
            <User
                username="Alice Avery"
                avatar="https://images.unsplash.com/photo-1511193311914-0346f16efe90?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1053&q=80"
                excerpt="I'm Alice. Lorem ipsum dolor sit amet, consectetur adipisicing elit."
            />
            <User
                username="Bobby Beetles"
                avatar="https://images.unsplash.com/photo-1571244112823-db09c790e924?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80"
                excerpt="I'm Bobby. Lorem ipsum dolor sit amet, consectetur adipisicing elit."
                />
        </Container>
    )
}